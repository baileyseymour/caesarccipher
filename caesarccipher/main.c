//
//  main.c
//  caesarccipher
//
//  Created by Bailey Seymour on 4/20/15.
//  Copyright (c) 2015 Bailey Seymour. All rights reserved.
//

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

const char *ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

const char *alphabet = "abcdefghijklmnopqrstuvwxyz";

int index_in_alphabet(const char letter)
{
    switch (letter) {
        case 'A':
        case 'a':
            return 0;
            break;
        case 'B':
        case 'b':
            return 1;
            break;
        case 'C':
        case 'c':
            return 2;
            break;
        case 'D':
        case 'd':
            return 3;
            break;
        case 'E':
        case 'e':
            return 4;
            break;
        case 'F':
        case 'f':
            return 5;
            break;
        case 'G':
        case 'g':
            return 6;
            break;
        case 'H':
        case 'h':
            return 7;
            break;
        case 'I':
        case 'i':
            return 8;
            break;
        case 'J':
        case 'j':
            return 9;
            break;
        case 'K':
        case 'k':
            return 10;
            break;
        case 'L':
        case 'l':
            return 11;
            break;
        case 'M':
        case 'm':
            return 12;
            break;
        case 'N':
        case 'n':
            return 13;
            break;
        case 'O':
        case 'o':
            return 14;
            break;
        case 'P':
        case 'p':
            return 15;
            break;
        case 'Q':
        case 'q':
            return 16;
            break;
        case 'R':
        case 'r':
            return 17;
            break;
        case 'S':
        case 's':
            return 18;
            break;
        case 'T':
        case 't':
            return 19;
            break;
        case 'U':
        case 'u':
            return 20;
            break;
        case 'V':
        case 'v':
            return 21;
            break;
        case 'W':
        case 'w':
            return 22;
            break;
        case 'X':
        case 'x':
            return 23;
            break;
        case 'Y':
        case 'y':
            return 24;
            break;
        case 'Z':
        case 'z':
            return 25;
            break;
            
        default:
            return 1000;
            break;
    }
    

}

int is_upper(char ch)
{
    if (ch >= 'A' && ch<= 'Z')
        return 1;
    else
        return 0;
}

const char *caesar(const char *original, int shift, int encipher)
{
    char *ns;
    
    for (int i=0; strlen(original) > i; i++) {
        int idx = index_in_alphabet(original[i]);
        
        
        if (idx == 1000)
        {
            ns[i] = original[i];
        }
        else if (encipher == 1)
        {
            
            if (is_upper(original[i]) == 0)
                ns[i] = alphabet[idx+shift];
            else
                ns[i] = ALPHABET[(idx)+shift];
        }
        else
        {
            
            if (is_upper(original[i]) == 0)
            {
                if (idx-shift < 0)
                {
                    int nidx = 26 + (idx - shift);
                    ns[i] = alphabet[nidx];
                }
                else
                    ns[i] = alphabet[idx-shift];
            }
            else
            {
                if (idx-shift < 0)
                {
                    int nidx = 26 + (idx - shift);
                    ns[i] = ALPHABET[nidx];
                }
                else
                    ns[i] = ALPHABET[idx-shift];
            }
        }
        
        
    }
    
    return ns;
}

int main(int argc,  char * const argv[]) {
    
    int option;
    int digit_optind = 0;
    const char *input = NULL;
    int shift = 1;
    int encipher = 0;
    
    while ( (option = getopt(argc, argv, "i:s:e")) != -1) {
        int this_option_optind = optind ? optind : 1;
        switch (option) {
            case '0':
            case '1':
            case '2':
                if (digit_optind != 0 && digit_optind != this_option_optind)
                    printf ("digits occur in two different argv-elements.\n");
                digit_optind = this_option_optind;
                printf ("option %c\n", option);
                break;
            case 'i':
                input = optarg;
                
                break;
            
            case 's':
                
                shift = atoi(optarg);
                
                break;
            case 'e':
                encipher = 1;
                
                break;
            case '?':
                break;
            default:
                printf ("?? getopt returned character code 0%o ??\n", option);
        }
    }
    
    if (argc < 2)
    {
        printf("Usage: caesarccipher -i <input text> -s <shift number> [-e (use encipher)]");
    }
    if (input)
    {
    if (strlen(input) > 0)
    {
        printf("%s\n", caesar(input, shift, encipher));
    }
    else {
        printf("no input specified.\n");
    }
    }
    else {
        printf("no input specified.\n");
    }

    
    
    
    return 0;
}
